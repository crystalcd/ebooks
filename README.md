# 学习电子书，仅供学习
```

|-- 文档
    |-- .gitignore
    |-- README.md
    |-- directoryList.md
    |-- go
    |   |-- Go语言实战.pdf
    |   |-- go语言圣经.pdf
    |-- java
    |   |-- spring
    |       |-- Spring5高级编程（第5版）.pdf
    |       |-- 深入浅出Spring Boot.pdf
    |-- linux
    |   |-- 鸟哥的linux私房菜.pdf
    |-- 中间件
    |   |-- RocketMQ实战与原理解析.pdf
    |-- 大数据
    |-- 微服务
    |   |-- Spring Cloud微服务实战.pdf
    |-- 计算机科学与技术
        |-- 算法导论中文第三版.pdf
        |-- 编译原理(龙书第2版).pdf


```